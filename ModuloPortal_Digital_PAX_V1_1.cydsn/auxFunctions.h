/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "common.h"
#include "memory.h"


void learnDevice(struct EnoTelegram tlgReceived);
void forgetDevice(void);
void buzzerNotification(uint8 mode);
void processTelegram(struct EnoTelegram telegram);
void addPending (uint8 auxTlg[5], uint8 channel);
void clearPending (uint8 auxTlg[5], uint8 channel);
uint8 checkType (uint8 data);
void checkPending(void);
void clearLowBatt(uint8 auxTlg[5], uint8 channel);
void factoryReset(void);

extern uint8 opMode;
extern uint8 learnResult;
extern uint8 forgetResult;
extern uint8 flagTimer;

extern struct EnoTelegram telegramRecvd;




/* [] END OF FILE */
