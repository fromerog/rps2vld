/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include <project.h>

#ifndef _COMMONH_
    

    
#define _COMMONH_

#define RESET_ENOCEAN_ON    EnoRESET_Write(1);     //se activa a nivel alto
#define RESET_ENOCEAN_OFF   EnoRESET_Write(0);

#define POWER_ENOCEAN_ON    EnoPWR_Write(0);     //se activa a nivel bajo
#define POWER_ENOCEAN_OFF   EnoPWR_Write(1);
    
#define CICLOS_ALARMA_NORMAL 5
#define CICLOS_ALARMA_ESTROB 30
#define CICLOS_ALARMA_WIRED 10

#define ON  1u
#define OFF 0u

#define TRUE    1u
#define FALSE   0u

#define BUZZER_ON   Alarm_Write(1u);
#define BUZZER_OFF  Alarm_Write(0u);

#define CHANNEL_1   1
#define CHANNEL_2   2
#define CHANNEL_3   3
#define CHANNEL_4   4
#define CHANNEL_5   5
#define CHANNEL_6   6



#define RPS 0xF6
#define BS1 0xD5
#define BS4 0xA5
#define VLD 0xD2
#define RESP 0x02
    
   
    

#define ENO_ON 1
#define ENO_OFF 0
    

    
#define OK 0x00
#define ERROR 0x01
#define TIMEOUT 0x02
    
uint8 recvdResult;
uint8 opMode;
uint8 flagTimer;
struct EnoTelegram telegramRecvd;
uint8 btAddr[6];
    

struct EnoTelegram
    {
        uint8 RORG;
        uint8 ID_TX[4];
        uint8 DATA[4];
        uint8 ID_RX[4];
        uint8 STATUS;
    };
    

    
#endif