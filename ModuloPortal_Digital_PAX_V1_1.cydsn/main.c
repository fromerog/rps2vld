/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include <project.h>

#include "EnOcean.h"
#include "common.h"
#include "auxFunctions.h"


#define cycleTime 100 //Tiempo para cada ciclo
#define WDT_COUNT0_MATCH (cycleTime*1000/31.25) //Calculado a partir de la freq. del ILO (32KHz)

#define NORMAL_MODE 0x00
#define LEARN_MODE 0x01

#define SINGLE_TOUCH 0x00
#define LONG_TOUCH 0x01

//MensajeENO Repetidor[8] =  { 0x55, 0x00, 0x03, 0x00, 0x05, 0x09, 0x01, 0x01};  // Comando REPETIDOR L1
//MensajeENO Repetidor[8] =  { 0x55, 0x00, 0x03, 0x00, 0x05, 0x09, 0x01, 0x02};  // Comando REPETIDOR L2

MensajeENO Mensaje_RPS[12] = { 0x55, 0x00, 0x07, 0x00, 0x01, 0xF6, 0xFF, 0xAA, 0xAA, 0xAA, 0xAA, 0x20};
MensajeENO Mensaje_VLD[16] = { 0x55, 0x00, 0x0B, 0x00, 0x01, 0xD2, 0xBB, 0xAA, 0xAA, 0xAA, 0xAA, 0xCC, 0xCC, 0xCC, 0xCC, 0x20};
//MensajeENO Mensaje_FULL[19] = { 0x55, 0x00, 0x07, 0x07, 0x01,                 Header
//                                0xF6, 0xF3, 0xAA, 0xAA, 0xAA, 0xAA, 0x20,     Data payload
//                                0x03, 0xAA, 0xBB, 0xCC, 0xDD, 0xFF, 0x00};    Optional data

void init();
void sendResp();

CY_ISR_PROTO(IsrEnoUART);
CY_ISR_PROTO(TIMER);
CY_ISR_PROTO(IsrWDT);
CY_ISR_PROTO(IsrInDig1);
CY_ISR_PROTO(INT_Button);

uint32 wdtCount = 0;
uint8 mode = 0;

int main()
{
    init();
    

    for(;;)
    {
       
    }
}



void init()
{
    CyGlobalIntEnable; 
    
    Clock_1_Enable();                       //inicia relojes
   
    
    TIMER_Init();             //inicia timer
    TIMER_Enable();
    TIMER_Stop();
    
      
    IsrEnoUART_StartEx(IsrEnoUART);
    isrTimer_StartEx(TIMER);
    IsrWDT_StartEx(IsrWDT);
    INT_Button_StartEx(INT_Button);
    
        
	CySysWdtSetMode(CY_SYS_WDT_COUNTER0, CY_SYS_WDT_MODE_INT);
	CySysWdtSetMatch(CY_SYS_WDT_COUNTER0, WDT_COUNT0_MATCH);
	CySysWdtSetClearOnMatch(CY_SYS_WDT_COUNTER0, 1u);
    
    CySysWdtDisable(CY_SYS_WDT_COUNTER0_MASK); //habilita el WDT
    
    load_IDs();
    
    OutDig_0_SetDriveMode(OutDig_0_DM_STRONG);
    Out_B_SetDriveMode(Out_B_DM_RES_DWN);
    Out_R_SetDriveMode(Out_R_DM_RES_DWN);
     
    OutDig_0_Write(0u);
    Out_R_Write(0u);
    Out_B_Write(0u);
        
    EnoceanON();
    CyDelay(10);
    //EnviarMensajeENO(Repetidor);    // Activando modo REPETIDOR
}

void checkTlg(struct EnoTelegram tlgReceived)
{
    uint8 tlgExist = FALSE; 
    uint8 i = 0;
    
    switch(tlgReceived.RORG)
    {
        case RPS:                     
            Mensaje_VLD[6]  = tlgReceived.DATA[0];
        
            Mensaje_VLD[7]  = tlgReceived.ID_TX[0];
            Mensaje_VLD[8]  = tlgReceived.ID_TX[1];
            Mensaje_VLD[9]  = tlgReceived.ID_TX[2];
            Mensaje_VLD[10] = tlgReceived.ID_TX[3];
            
            EnviarMensajeENO(Mensaje_VLD);    // Repitiendo mensaje recibido/            break;   
    
        case VLD:
            Mensaje_RPS[6]  = tlgReceived.DATA[0];  
            
            EnviarMensajeENO(Mensaje_RPS);    // Repitiendo mensaje recibido
            break;
           
        //default:
    }
        
//    if ((tlgReceived.RORG == 0xF6) && ((tlgReceived.DATA[0] == 0x10)||(tlgReceived.DATA[0] == 0x30)||(tlgReceived.DATA[0] == 0x50)||(tlgReceived.DATA[0] == 0x70)||(tlgReceived.DATA[0] == 0x80)))
    if (tlgReceived.RORG == 0xF6)
    {
        tlgExist = check_telegram_ID(tlgReceived);
        
        if (mode == NORMAL_MODE)
        {
            if (tlgExist)
            {
                if (tlgReceived.DATA[0] == 0xAF)
                    OutDig_0_Write(1u);
                
                if (tlgReceived.DATA[0] == 0xF2)
                    OutDig_0_Write(0u);
                    
//                OutDig_0_Write(1u);
//                if (!InDig_1_Read())
//                {
//                  Enciendo primero la pantalla
//    				Out_R_Write(1u);
//    				CyDelay(1500);
//    				Out_R_Write(0u);
//    				Out_R_SetDriveMode(OutDig_2_DM_ALG_HIZ);

//                    CyDelay(5000);
//                }
//                Pulso el botón de apertura
//                Out_B_Write(1u);
//                CyDelay(1500);
//                Out_B_Write(0u);
//                Out_B_SetDriveMode(OutDig_2_DM_ALG_HIZ);    
                
//                Activa relé durante 10 segundos para dar tiempo a que llame al portal y reciba telegram
//                OutDig_0_Write(1u);
//                CyDelay(2000);
//                OutDig_0_Write(0u);
                
//                CyDelay(1000);
//                OutDig_2_Write(1u);
//                OutDig_2_SetDriveMode(OutDig_2_DM_ALG_HIZ);
                sendResp();
//                OutDig_0_Write(0u);
            }
        }
        else if (mode == LEARN_MODE)
        {
            uint8 result = ERROR;
            mode = NORMAL_MODE;
            
            if (!tlgExist)
                result = learn_ID(tlgReceived);
                
            if (result == OK)
                sendResp();
                
            OutDig_1_Write(0u);
        }
    }
    
}

void sendResp()
{
        OutDig_1_Write(1u);
        
        CyDelay(100);
        
        CySysLvdEnable(CY_LVD_THRESHOLD_2_80_V);//Pongo el limite para la interrupción de batería baja
                          //en este caso lo pongo a 2,8 V (Tensión mínima a la que funciona el EnOcean)
        
        CyDelay(100);
        
        OutDig_1_Write(0u);
        
        CyDelay(100);
        
        if(CySysLvdGetInterruptSource()==CY_SYS_LVD_INT) //si hay una interrupción por batería baja
            {
                CySysLvdDisable(); //deshabilito el controlador de batería baja
                CySysLvdClearInterrupt(); //limpio el flag de la interrupción
                TipoMensajeENO(1); //Mando un mensaje de Enocean    
            }
        else
            {
                TipoMensajeENO(2);
            }    
    
}

/* [] END OF FILE */

CY_ISR(IsrEnoUART)
{
    
    telegramRecvd = receiveEnocean();
    
    if (telegramRecvd.RORG != 0x00)
    {
        checkTlg(telegramRecvd);
    }
    
    uint32 source = EnoUART_GetRxInterruptSourceMasked();
    EnoUART_ClearRxInterruptSource(source);
    IsrEnoUART_ClearPending(); //Limpia la interrupcion
}

CY_ISR(TIMER)
{
   
    mode = NORMAL_MODE;
    
    OutDig_1_Write(0u);
    
    TIMER_Stop();
    
    TIMER_ClearInterrupt(TIMER_INTR_MASK_TC);
}


CY_ISR(IsrWDT)
{
    wdtCount++;
        
    CySysWdtClearInterrupt(CY_SYS_WDT_COUNTER0_INT);
    IsrWDT_ClearPending(); //Limpia la interrupcion
}

CY_ISR(INT_Button)
{
    
    
    TIMER_WriteCounter(0);
   
    isrTimer_Stop();
    TIMER_Start();
    while (!Button_Read())
    {
        if (TIMER_ReadCounter() > 5500)
            break;
        
    }
    
    
    TIMER_Stop();
    isrTimer_StartEx(TIMER);
    


    if (TIMER_ReadCounter()<5000)
        {
            mode = LEARN_MODE;
            OutDig_1_Write(1u);
        }
    else
        {
            forget_all_ID();
            OutDig_1_Write(1u);
            CyDelay(500);
            OutDig_1_Write(0u);
            CyDelay(500);
            OutDig_1_Write(1u);
            CyDelay(500);
            OutDig_1_Write(0u);
            CyDelay(500);
            OutDig_1_Write(1u);
            CyDelay(500);
            OutDig_1_Write(0u);
            CyDelay(500); 
        }
        
    TIMER_Start();

    
    Button_ClearInterrupt();
    INT_Button_ClearPending();  
}