/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "common.h"


    /* Defines ROWS */
#define CY_FLASH_ROW_ROJO       (CY_FLASH_NUMBER_ROWS - 64u)

/* Defines absolut address of ROW */
#define CY_TEST_FLASH_ADDR      (CY_TEST_FLASH_ROW * CY_FLASH_SIZEOF_ROW)



volatile uint8 sourceROJO;

uint8 *ADDR_ROW_ROJO;

uint8 IDs_RECOVERED_ROJO[CY_FLASH_SIZEOF_ROW];


struct EnoTelegram;

extern uint8 CFG_ROJO[4];

void load_IDs();
void reset_flash();
uint8 learn_ID(struct EnoTelegram telegram);
void forget_one_ID (struct EnoTelegram telegram);
void forget_all_ID(void);
uint8 check_telegram_ID(struct EnoTelegram telegram);
void erase_all();
uint8 auxConv(uint8 val);

/* [] END OF FILE */
