/*******************************************************************************
* File Name: Alarm.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Alarm_H) /* Pins Alarm_H */
#define CY_PINS_Alarm_H

#include "cytypes.h"
#include "cyfitter.h"
#include "Alarm_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} Alarm_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   Alarm_Read(void);
void    Alarm_Write(uint8 value);
uint8   Alarm_ReadDataReg(void);
#if defined(Alarm__PC) || (CY_PSOC4_4200L) 
    void    Alarm_SetDriveMode(uint8 mode);
#endif
void    Alarm_SetInterruptMode(uint16 position, uint16 mode);
uint8   Alarm_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void Alarm_Sleep(void); 
void Alarm_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(Alarm__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define Alarm_DRIVE_MODE_BITS        (3)
    #define Alarm_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - Alarm_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the Alarm_SetDriveMode() function.
         *  @{
         */
        #define Alarm_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define Alarm_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define Alarm_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define Alarm_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define Alarm_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define Alarm_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define Alarm_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define Alarm_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define Alarm_MASK               Alarm__MASK
#define Alarm_SHIFT              Alarm__SHIFT
#define Alarm_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in Alarm_SetInterruptMode() function.
     *  @{
     */
        #define Alarm_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define Alarm_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define Alarm_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define Alarm_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(Alarm__SIO)
    #define Alarm_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(Alarm__PC) && (CY_PSOC4_4200L)
    #define Alarm_USBIO_ENABLE               ((uint32)0x80000000u)
    #define Alarm_USBIO_DISABLE              ((uint32)(~Alarm_USBIO_ENABLE))
    #define Alarm_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define Alarm_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define Alarm_USBIO_ENTER_SLEEP          ((uint32)((1u << Alarm_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << Alarm_USBIO_SUSPEND_DEL_SHIFT)))
    #define Alarm_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << Alarm_USBIO_SUSPEND_SHIFT)))
    #define Alarm_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << Alarm_USBIO_SUSPEND_DEL_SHIFT)))
    #define Alarm_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(Alarm__PC)
    /* Port Configuration */
    #define Alarm_PC                 (* (reg32 *) Alarm__PC)
#endif
/* Pin State */
#define Alarm_PS                     (* (reg32 *) Alarm__PS)
/* Data Register */
#define Alarm_DR                     (* (reg32 *) Alarm__DR)
/* Input Buffer Disable Override */
#define Alarm_INP_DIS                (* (reg32 *) Alarm__PC2)

/* Interrupt configuration Registers */
#define Alarm_INTCFG                 (* (reg32 *) Alarm__INTCFG)
#define Alarm_INTSTAT                (* (reg32 *) Alarm__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define Alarm_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(Alarm__SIO)
    #define Alarm_SIO_REG            (* (reg32 *) Alarm__SIO)
#endif /* (Alarm__SIO_CFG) */

/* USBIO registers */
#if !defined(Alarm__PC) && (CY_PSOC4_4200L)
    #define Alarm_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define Alarm_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define Alarm_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define Alarm_DRIVE_MODE_SHIFT       (0x00u)
#define Alarm_DRIVE_MODE_MASK        (0x07u << Alarm_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins Alarm_H */


/* [] END OF FILE */
