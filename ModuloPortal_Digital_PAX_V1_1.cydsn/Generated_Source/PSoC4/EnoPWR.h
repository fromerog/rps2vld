/*******************************************************************************
* File Name: EnoPWR.h  
* Version 2.20
*
* Description:
*  This file contains Pin function prototypes and register defines
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_EnoPWR_H) /* Pins EnoPWR_H */
#define CY_PINS_EnoPWR_H

#include "cytypes.h"
#include "cyfitter.h"
#include "EnoPWR_aliases.h"


/***************************************
*     Data Struct Definitions
***************************************/

/**
* \addtogroup group_structures
* @{
*/
    
/* Structure for sleep mode support */
typedef struct
{
    uint32 pcState; /**< State of the port control register */
    uint32 sioState; /**< State of the SIO configuration */
    uint32 usbState; /**< State of the USBIO regulator */
} EnoPWR_BACKUP_STRUCT;

/** @} structures */


/***************************************
*        Function Prototypes             
***************************************/
/**
* \addtogroup group_general
* @{
*/
uint8   EnoPWR_Read(void);
void    EnoPWR_Write(uint8 value);
uint8   EnoPWR_ReadDataReg(void);
#if defined(EnoPWR__PC) || (CY_PSOC4_4200L) 
    void    EnoPWR_SetDriveMode(uint8 mode);
#endif
void    EnoPWR_SetInterruptMode(uint16 position, uint16 mode);
uint8   EnoPWR_ClearInterrupt(void);
/** @} general */

/**
* \addtogroup group_power
* @{
*/
void EnoPWR_Sleep(void); 
void EnoPWR_Wakeup(void);
/** @} power */


/***************************************
*           API Constants        
***************************************/
#if defined(EnoPWR__PC) || (CY_PSOC4_4200L) 
    /* Drive Modes */
    #define EnoPWR_DRIVE_MODE_BITS        (3)
    #define EnoPWR_DRIVE_MODE_IND_MASK    (0xFFFFFFFFu >> (32 - EnoPWR_DRIVE_MODE_BITS))

    /**
    * \addtogroup group_constants
    * @{
    */
        /** \addtogroup driveMode Drive mode constants
         * \brief Constants to be passed as "mode" parameter in the EnoPWR_SetDriveMode() function.
         *  @{
         */
        #define EnoPWR_DM_ALG_HIZ         (0x00u) /**< \brief High Impedance Analog   */
        #define EnoPWR_DM_DIG_HIZ         (0x01u) /**< \brief High Impedance Digital  */
        #define EnoPWR_DM_RES_UP          (0x02u) /**< \brief Resistive Pull Up       */
        #define EnoPWR_DM_RES_DWN         (0x03u) /**< \brief Resistive Pull Down     */
        #define EnoPWR_DM_OD_LO           (0x04u) /**< \brief Open Drain, Drives Low  */
        #define EnoPWR_DM_OD_HI           (0x05u) /**< \brief Open Drain, Drives High */
        #define EnoPWR_DM_STRONG          (0x06u) /**< \brief Strong Drive            */
        #define EnoPWR_DM_RES_UPDWN       (0x07u) /**< \brief Resistive Pull Up/Down  */
        /** @} driveMode */
    /** @} group_constants */
#endif

/* Digital Port Constants */
#define EnoPWR_MASK               EnoPWR__MASK
#define EnoPWR_SHIFT              EnoPWR__SHIFT
#define EnoPWR_WIDTH              1u

/**
* \addtogroup group_constants
* @{
*/
    /** \addtogroup intrMode Interrupt constants
     * \brief Constants to be passed as "mode" parameter in EnoPWR_SetInterruptMode() function.
     *  @{
     */
        #define EnoPWR_INTR_NONE      ((uint16)(0x0000u)) /**< \brief Disabled             */
        #define EnoPWR_INTR_RISING    ((uint16)(0x5555u)) /**< \brief Rising edge trigger  */
        #define EnoPWR_INTR_FALLING   ((uint16)(0xaaaau)) /**< \brief Falling edge trigger */
        #define EnoPWR_INTR_BOTH      ((uint16)(0xffffu)) /**< \brief Both edge trigger    */
    /** @} intrMode */
/** @} group_constants */

/* SIO LPM definition */
#if defined(EnoPWR__SIO)
    #define EnoPWR_SIO_LPM_MASK       (0x03u)
#endif

/* USBIO definitions */
#if !defined(EnoPWR__PC) && (CY_PSOC4_4200L)
    #define EnoPWR_USBIO_ENABLE               ((uint32)0x80000000u)
    #define EnoPWR_USBIO_DISABLE              ((uint32)(~EnoPWR_USBIO_ENABLE))
    #define EnoPWR_USBIO_SUSPEND_SHIFT        CYFLD_USBDEVv2_USB_SUSPEND__OFFSET
    #define EnoPWR_USBIO_SUSPEND_DEL_SHIFT    CYFLD_USBDEVv2_USB_SUSPEND_DEL__OFFSET
    #define EnoPWR_USBIO_ENTER_SLEEP          ((uint32)((1u << EnoPWR_USBIO_SUSPEND_SHIFT) \
                                                        | (1u << EnoPWR_USBIO_SUSPEND_DEL_SHIFT)))
    #define EnoPWR_USBIO_EXIT_SLEEP_PH1       ((uint32)~((uint32)(1u << EnoPWR_USBIO_SUSPEND_SHIFT)))
    #define EnoPWR_USBIO_EXIT_SLEEP_PH2       ((uint32)~((uint32)(1u << EnoPWR_USBIO_SUSPEND_DEL_SHIFT)))
    #define EnoPWR_USBIO_CR1_OFF              ((uint32)0xfffffffeu)
#endif


/***************************************
*             Registers        
***************************************/
/* Main Port Registers */
#if defined(EnoPWR__PC)
    /* Port Configuration */
    #define EnoPWR_PC                 (* (reg32 *) EnoPWR__PC)
#endif
/* Pin State */
#define EnoPWR_PS                     (* (reg32 *) EnoPWR__PS)
/* Data Register */
#define EnoPWR_DR                     (* (reg32 *) EnoPWR__DR)
/* Input Buffer Disable Override */
#define EnoPWR_INP_DIS                (* (reg32 *) EnoPWR__PC2)

/* Interrupt configuration Registers */
#define EnoPWR_INTCFG                 (* (reg32 *) EnoPWR__INTCFG)
#define EnoPWR_INTSTAT                (* (reg32 *) EnoPWR__INTSTAT)

/* "Interrupt cause" register for Combined Port Interrupt (AllPortInt) in GSRef component */
#if defined (CYREG_GPIO_INTR_CAUSE)
    #define EnoPWR_INTR_CAUSE         (* (reg32 *) CYREG_GPIO_INTR_CAUSE)
#endif

/* SIO register */
#if defined(EnoPWR__SIO)
    #define EnoPWR_SIO_REG            (* (reg32 *) EnoPWR__SIO)
#endif /* (EnoPWR__SIO_CFG) */

/* USBIO registers */
#if !defined(EnoPWR__PC) && (CY_PSOC4_4200L)
    #define EnoPWR_USB_POWER_REG       (* (reg32 *) CYREG_USBDEVv2_USB_POWER_CTRL)
    #define EnoPWR_CR1_REG             (* (reg32 *) CYREG_USBDEVv2_CR1)
    #define EnoPWR_USBIO_CTRL_REG      (* (reg32 *) CYREG_USBDEVv2_USB_USBIO_CTRL)
#endif    
    
    
/***************************************
* The following code is DEPRECATED and 
* must not be used in new designs.
***************************************/
/**
* \addtogroup group_deprecated
* @{
*/
#define EnoPWR_DRIVE_MODE_SHIFT       (0x00u)
#define EnoPWR_DRIVE_MODE_MASK        (0x07u << EnoPWR_DRIVE_MODE_SHIFT)
/** @} deprecated */

#endif /* End Pins EnoPWR_H */


/* [] END OF FILE */
