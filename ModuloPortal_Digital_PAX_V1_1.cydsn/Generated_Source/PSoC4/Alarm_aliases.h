/*******************************************************************************
* File Name: Alarm.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_Alarm_ALIASES_H) /* Pins Alarm_ALIASES_H */
#define CY_PINS_Alarm_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define Alarm_0			(Alarm__0__PC)
#define Alarm_0_PS		(Alarm__0__PS)
#define Alarm_0_PC		(Alarm__0__PC)
#define Alarm_0_DR		(Alarm__0__DR)
#define Alarm_0_SHIFT	(Alarm__0__SHIFT)
#define Alarm_0_INTR	((uint16)((uint16)0x0003u << (Alarm__0__SHIFT*2u)))

#define Alarm_INTR_ALL	 ((uint16)(Alarm_0_INTR))


#endif /* End Pins Alarm_ALIASES_H */


/* [] END OF FILE */
