/*******************************************************************************
* File Name: InDig_0.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_InDig_0_ALIASES_H) /* Pins InDig_0_ALIASES_H */
#define CY_PINS_InDig_0_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define InDig_0_0			(InDig_0__0__PC)
#define InDig_0_0_PS		(InDig_0__0__PS)
#define InDig_0_0_PC		(InDig_0__0__PC)
#define InDig_0_0_DR		(InDig_0__0__DR)
#define InDig_0_0_SHIFT	(InDig_0__0__SHIFT)
#define InDig_0_0_INTR	((uint16)((uint16)0x0003u << (InDig_0__0__SHIFT*2u)))

#define InDig_0_INTR_ALL	 ((uint16)(InDig_0_0_INTR))


#endif /* End Pins InDig_0_ALIASES_H */


/* [] END OF FILE */
