/*******************************************************************************
* File Name: INT_Tick_1ms.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_INT_Tick_1ms_H)
#define CY_ISR_INT_Tick_1ms_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void INT_Tick_1ms_Start(void);
void INT_Tick_1ms_StartEx(cyisraddress address);
void INT_Tick_1ms_Stop(void);

CY_ISR_PROTO(INT_Tick_1ms_Interrupt);

void INT_Tick_1ms_SetVector(cyisraddress address);
cyisraddress INT_Tick_1ms_GetVector(void);

void INT_Tick_1ms_SetPriority(uint8 priority);
uint8 INT_Tick_1ms_GetPriority(void);

void INT_Tick_1ms_Enable(void);
uint8 INT_Tick_1ms_GetState(void);
void INT_Tick_1ms_Disable(void);

void INT_Tick_1ms_SetPending(void);
void INT_Tick_1ms_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the INT_Tick_1ms ISR. */
#define INT_Tick_1ms_INTC_VECTOR            ((reg32 *) INT_Tick_1ms__INTC_VECT)

/* Address of the INT_Tick_1ms ISR priority. */
#define INT_Tick_1ms_INTC_PRIOR             ((reg32 *) INT_Tick_1ms__INTC_PRIOR_REG)

/* Priority of the INT_Tick_1ms interrupt. */
#define INT_Tick_1ms_INTC_PRIOR_NUMBER      INT_Tick_1ms__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable INT_Tick_1ms interrupt. */
#define INT_Tick_1ms_INTC_SET_EN            ((reg32 *) INT_Tick_1ms__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the INT_Tick_1ms interrupt. */
#define INT_Tick_1ms_INTC_CLR_EN            ((reg32 *) INT_Tick_1ms__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the INT_Tick_1ms interrupt state to pending. */
#define INT_Tick_1ms_INTC_SET_PD            ((reg32 *) INT_Tick_1ms__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the INT_Tick_1ms interrupt. */
#define INT_Tick_1ms_INTC_CLR_PD            ((reg32 *) INT_Tick_1ms__INTC_CLR_PD_REG)



#endif /* CY_ISR_INT_Tick_1ms_H */


/* [] END OF FILE */
