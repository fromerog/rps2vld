/***************************************************************************//**
* \file EnoUART_BOOT.h
* \version 4.0
*
* \brief
*  This file provides constants and parameter values of the bootloader
*  communication APIs for the SCB Component.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2014-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_BOOT_EnoUART_H)
#define CY_SCB_BOOT_EnoUART_H

#include "EnoUART_PVT.h"

#if (EnoUART_SCB_MODE_I2C_INC)
    #include "EnoUART_I2C.h"
#endif /* (EnoUART_SCB_MODE_I2C_INC) */

#if (EnoUART_SCB_MODE_EZI2C_INC)
    #include "EnoUART_EZI2C.h"
#endif /* (EnoUART_SCB_MODE_EZI2C_INC) */

#if (EnoUART_SCB_MODE_SPI_INC || EnoUART_SCB_MODE_UART_INC)
    #include "EnoUART_SPI_UART.h"
#endif /* (EnoUART_SCB_MODE_SPI_INC || EnoUART_SCB_MODE_UART_INC) */


/***************************************
*  Conditional Compilation Parameters
****************************************/

/* Bootloader communication interface enable */
#define EnoUART_BTLDR_COMM_ENABLED ((CYDEV_BOOTLOADER_IO_COMP == CyBtldr_EnoUART) || \
                                             (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_Custom_Interface))

/* Enable I2C bootloader communication */
#if (EnoUART_SCB_MODE_I2C_INC)
    #define EnoUART_I2C_BTLDR_COMM_ENABLED     (EnoUART_BTLDR_COMM_ENABLED && \
                                                            (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             EnoUART_I2C_SLAVE_CONST))
#else
     #define EnoUART_I2C_BTLDR_COMM_ENABLED    (0u)
#endif /* (EnoUART_SCB_MODE_I2C_INC) */

/* EZI2C does not support bootloader communication. Provide empty APIs */
#if (EnoUART_SCB_MODE_EZI2C_INC)
    #define EnoUART_EZI2C_BTLDR_COMM_ENABLED   (EnoUART_BTLDR_COMM_ENABLED && \
                                                         EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
#else
    #define EnoUART_EZI2C_BTLDR_COMM_ENABLED   (0u)
#endif /* (EnoUART_EZI2C_BTLDR_COMM_ENABLED) */

/* Enable SPI bootloader communication */
#if (EnoUART_SCB_MODE_SPI_INC)
    #define EnoUART_SPI_BTLDR_COMM_ENABLED     (EnoUART_BTLDR_COMM_ENABLED && \
                                                            (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             EnoUART_SPI_SLAVE_CONST))
#else
        #define EnoUART_SPI_BTLDR_COMM_ENABLED (0u)
#endif /* (EnoUART_SPI_BTLDR_COMM_ENABLED) */

/* Enable UART bootloader communication */
#if (EnoUART_SCB_MODE_UART_INC)
       #define EnoUART_UART_BTLDR_COMM_ENABLED    (EnoUART_BTLDR_COMM_ENABLED && \
                                                            (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG || \
                                                             (EnoUART_UART_RX_DIRECTION && \
                                                              EnoUART_UART_TX_DIRECTION)))
#else
     #define EnoUART_UART_BTLDR_COMM_ENABLED   (0u)
#endif /* (EnoUART_UART_BTLDR_COMM_ENABLED) */

/* Enable bootloader communication */
#define EnoUART_BTLDR_COMM_MODE_ENABLED    (EnoUART_I2C_BTLDR_COMM_ENABLED   || \
                                                     EnoUART_SPI_BTLDR_COMM_ENABLED   || \
                                                     EnoUART_EZI2C_BTLDR_COMM_ENABLED || \
                                                     EnoUART_UART_BTLDR_COMM_ENABLED)


/***************************************
*        Function Prototypes
***************************************/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_I2C_BTLDR_COMM_ENABLED)
    /* I2C Bootloader physical layer functions */
    void EnoUART_I2CCyBtldrCommStart(void);
    void EnoUART_I2CCyBtldrCommStop (void);
    void EnoUART_I2CCyBtldrCommReset(void);
    cystatus EnoUART_I2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus EnoUART_I2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map I2C specific bootloader communication APIs to SCB specific APIs */
    #if (EnoUART_SCB_MODE_I2C_CONST_CFG)
        #define EnoUART_CyBtldrCommStart   EnoUART_I2CCyBtldrCommStart
        #define EnoUART_CyBtldrCommStop    EnoUART_I2CCyBtldrCommStop
        #define EnoUART_CyBtldrCommReset   EnoUART_I2CCyBtldrCommReset
        #define EnoUART_CyBtldrCommRead    EnoUART_I2CCyBtldrCommRead
        #define EnoUART_CyBtldrCommWrite   EnoUART_I2CCyBtldrCommWrite
    #endif /* (EnoUART_SCB_MODE_I2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_I2C_BTLDR_COMM_ENABLED) */


#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EZI2C_BTLDR_COMM_ENABLED)
    /* Bootloader physical layer functions */
    void EnoUART_EzI2CCyBtldrCommStart(void);
    void EnoUART_EzI2CCyBtldrCommStop (void);
    void EnoUART_EzI2CCyBtldrCommReset(void);
    cystatus EnoUART_EzI2CCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus EnoUART_EzI2CCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map EZI2C specific bootloader communication APIs to SCB specific APIs */
    #if (EnoUART_SCB_MODE_EZI2C_CONST_CFG)
        #define EnoUART_CyBtldrCommStart   EnoUART_EzI2CCyBtldrCommStart
        #define EnoUART_CyBtldrCommStop    EnoUART_EzI2CCyBtldrCommStop
        #define EnoUART_CyBtldrCommReset   EnoUART_EzI2CCyBtldrCommReset
        #define EnoUART_CyBtldrCommRead    EnoUART_EzI2CCyBtldrCommRead
        #define EnoUART_CyBtldrCommWrite   EnoUART_EzI2CCyBtldrCommWrite
    #endif /* (EnoUART_SCB_MODE_EZI2C_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_EZI2C_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_SPI_BTLDR_COMM_ENABLED)
    /* SPI Bootloader physical layer functions */
    void EnoUART_SpiCyBtldrCommStart(void);
    void EnoUART_SpiCyBtldrCommStop (void);
    void EnoUART_SpiCyBtldrCommReset(void);
    cystatus EnoUART_SpiCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus EnoUART_SpiCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map SPI specific bootloader communication APIs to SCB specific APIs */
    #if (EnoUART_SCB_MODE_SPI_CONST_CFG)
        #define EnoUART_CyBtldrCommStart   EnoUART_SpiCyBtldrCommStart
        #define EnoUART_CyBtldrCommStop    EnoUART_SpiCyBtldrCommStop
        #define EnoUART_CyBtldrCommReset   EnoUART_SpiCyBtldrCommReset
        #define EnoUART_CyBtldrCommRead    EnoUART_SpiCyBtldrCommRead
        #define EnoUART_CyBtldrCommWrite   EnoUART_SpiCyBtldrCommWrite
    #endif /* (EnoUART_SCB_MODE_SPI_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_SPI_BTLDR_COMM_ENABLED) */

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_UART_BTLDR_COMM_ENABLED)
    /* UART Bootloader physical layer functions */
    void EnoUART_UartCyBtldrCommStart(void);
    void EnoUART_UartCyBtldrCommStop (void);
    void EnoUART_UartCyBtldrCommReset(void);
    cystatus EnoUART_UartCyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    cystatus EnoUART_UartCyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);

    /* Map UART specific bootloader communication APIs to SCB specific APIs */
    #if (EnoUART_SCB_MODE_UART_CONST_CFG)
        #define EnoUART_CyBtldrCommStart   EnoUART_UartCyBtldrCommStart
        #define EnoUART_CyBtldrCommStop    EnoUART_UartCyBtldrCommStop
        #define EnoUART_CyBtldrCommReset   EnoUART_UartCyBtldrCommReset
        #define EnoUART_CyBtldrCommRead    EnoUART_UartCyBtldrCommRead
        #define EnoUART_CyBtldrCommWrite   EnoUART_UartCyBtldrCommWrite
    #endif /* (EnoUART_SCB_MODE_UART_CONST_CFG) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_UART_BTLDR_COMM_ENABLED) */

/**
* \addtogroup group_bootloader
* @{
*/

#if defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_BTLDR_COMM_ENABLED)
    #if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
        /* Bootloader physical layer functions */
        void EnoUART_CyBtldrCommStart(void);
        void EnoUART_CyBtldrCommStop (void);
        void EnoUART_CyBtldrCommReset(void);
        cystatus EnoUART_CyBtldrCommRead       (uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
        cystatus EnoUART_CyBtldrCommWrite(const uint8 pData[], uint16 size, uint16 * count, uint8 timeOut);
    #endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

    /* Map SCB specific bootloader communication APIs to common APIs */
    #if (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_EnoUART)
        #define CyBtldrCommStart    EnoUART_CyBtldrCommStart
        #define CyBtldrCommStop     EnoUART_CyBtldrCommStop
        #define CyBtldrCommReset    EnoUART_CyBtldrCommReset
        #define CyBtldrCommWrite    EnoUART_CyBtldrCommWrite
        #define CyBtldrCommRead     EnoUART_CyBtldrCommRead
    #endif /* (CYDEV_BOOTLOADER_IO_COMP == CyBtldr_EnoUART) */

#endif /* defined(CYDEV_BOOTLOADER_IO_COMP) && (EnoUART_BTLDR_COMM_ENABLED) */

/** @} group_bootloader */

/***************************************
*           API Constants
***************************************/

/* Timeout unit in milliseconds */
#define EnoUART_WAIT_1_MS  (1u)

/* Return number of bytes to copy into bootloader buffer */
#define EnoUART_BYTES_TO_COPY(actBufSize, bufSize) \
                            ( ((uint32)(actBufSize) < (uint32)(bufSize)) ? \
                                ((uint32) (actBufSize)) : ((uint32) (bufSize)) )

/* Size of Read/Write buffers for I2C bootloader  */
#define EnoUART_I2C_BTLDR_SIZEOF_READ_BUFFER   (64u)
#define EnoUART_I2C_BTLDR_SIZEOF_WRITE_BUFFER  (64u)

/* Byte to byte time interval: calculated basing on current component
* data rate configuration, can be defined in project if required.
*/
#ifndef EnoUART_SPI_BYTE_TO_BYTE
    #define EnoUART_SPI_BYTE_TO_BYTE   (160u)
#endif

/* Byte to byte time interval: calculated basing on current component
* baud rate configuration, can be defined in the project if required.
*/
#ifndef EnoUART_UART_BYTE_TO_BYTE
    #define EnoUART_UART_BYTE_TO_BYTE  (346u)
#endif /* EnoUART_UART_BYTE_TO_BYTE */

#endif /* (CY_SCB_BOOT_EnoUART_H) */


/* [] END OF FILE */
