/*******************************************************************************
* File Name: INT_Buttons.h
* Version 1.70
*
*  Description:
*   Provides the function definitions for the Interrupt Controller.
*
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/
#if !defined(CY_ISR_INT_Buttons_H)
#define CY_ISR_INT_Buttons_H


#include <cytypes.h>
#include <cyfitter.h>

/* Interrupt Controller API. */
void INT_Buttons_Start(void);
void INT_Buttons_StartEx(cyisraddress address);
void INT_Buttons_Stop(void);

CY_ISR_PROTO(INT_Buttons_Interrupt);

void INT_Buttons_SetVector(cyisraddress address);
cyisraddress INT_Buttons_GetVector(void);

void INT_Buttons_SetPriority(uint8 priority);
uint8 INT_Buttons_GetPriority(void);

void INT_Buttons_Enable(void);
uint8 INT_Buttons_GetState(void);
void INT_Buttons_Disable(void);

void INT_Buttons_SetPending(void);
void INT_Buttons_ClearPending(void);


/* Interrupt Controller Constants */

/* Address of the INTC.VECT[x] register that contains the Address of the INT_Buttons ISR. */
#define INT_Buttons_INTC_VECTOR            ((reg32 *) INT_Buttons__INTC_VECT)

/* Address of the INT_Buttons ISR priority. */
#define INT_Buttons_INTC_PRIOR             ((reg32 *) INT_Buttons__INTC_PRIOR_REG)

/* Priority of the INT_Buttons interrupt. */
#define INT_Buttons_INTC_PRIOR_NUMBER      INT_Buttons__INTC_PRIOR_NUM

/* Address of the INTC.SET_EN[x] byte to bit enable INT_Buttons interrupt. */
#define INT_Buttons_INTC_SET_EN            ((reg32 *) INT_Buttons__INTC_SET_EN_REG)

/* Address of the INTC.CLR_EN[x] register to bit clear the INT_Buttons interrupt. */
#define INT_Buttons_INTC_CLR_EN            ((reg32 *) INT_Buttons__INTC_CLR_EN_REG)

/* Address of the INTC.SET_PD[x] register to set the INT_Buttons interrupt state to pending. */
#define INT_Buttons_INTC_SET_PD            ((reg32 *) INT_Buttons__INTC_SET_PD_REG)

/* Address of the INTC.CLR_PD[x] register to clear the INT_Buttons interrupt. */
#define INT_Buttons_INTC_CLR_PD            ((reg32 *) INT_Buttons__INTC_CLR_PD_REG)



#endif /* CY_ISR_INT_Buttons_H */


/* [] END OF FILE */
