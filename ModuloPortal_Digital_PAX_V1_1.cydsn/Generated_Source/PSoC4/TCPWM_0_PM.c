/*******************************************************************************
* File Name: TCPWM_0_PM.c
* Version 2.10
*
* Description:
*  This file contains the setup, control, and status commands to support
*  the component operations in the low power mode.
*
* Note:
*  None
*
********************************************************************************
* Copyright 2013-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "TCPWM_0.h"

static TCPWM_0_BACKUP_STRUCT TCPWM_0_backup;


/*******************************************************************************
* Function Name: TCPWM_0_SaveConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to save here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void TCPWM_0_SaveConfig(void)
{

}


/*******************************************************************************
* Function Name: TCPWM_0_Sleep
********************************************************************************
*
* Summary:
*  Stops the component operation and saves the user configuration.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void TCPWM_0_Sleep(void)
{
    if(0u != (TCPWM_0_BLOCK_CONTROL_REG & TCPWM_0_MASK))
    {
        TCPWM_0_backup.enableState = 1u;
    }
    else
    {
        TCPWM_0_backup.enableState = 0u;
    }

    TCPWM_0_Stop();
    TCPWM_0_SaveConfig();
}


/*******************************************************************************
* Function Name: TCPWM_0_RestoreConfig
********************************************************************************
*
* Summary:
*  All configuration registers are retention. Nothing to restore here.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void TCPWM_0_RestoreConfig(void)
{

}


/*******************************************************************************
* Function Name: TCPWM_0_Wakeup
********************************************************************************
*
* Summary:
*  Restores the user configuration and restores the enable state.
*
* Parameters:
*  None
*
* Return:
*  None
*
*******************************************************************************/
void TCPWM_0_Wakeup(void)
{
    TCPWM_0_RestoreConfig();

    if(0u != TCPWM_0_backup.enableState)
    {
        TCPWM_0_Enable();
    }
}


/* [] END OF FILE */
