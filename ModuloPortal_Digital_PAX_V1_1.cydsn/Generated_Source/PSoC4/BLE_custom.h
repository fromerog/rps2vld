/***************************************************************************//**
* \file CYBLE_custom.h
* \version 3.40
* 
* \brief
*  Contains the function prototypes and constants for the Custom Service.
* 
********************************************************************************
* \copyright
* Copyright 2014-2016, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_BLE_CYBLE_CUSTOM_H)
#define CY_BLE_CYBLE_CUSTOM_H

#include "BLE_gatt.h"


/***************************************
* Conditional Compilation Parameters
***************************************/

/* Maximum supported Custom Services */
#define CYBLE_CUSTOMS_SERVICE_COUNT                  (0x01u)
#define CYBLE_CUSTOMC_SERVICE_COUNT                  (0x00u)
#define CYBLE_CUSTOM_SERVICE_CHAR_COUNT              (0x0Bu)
#define CYBLE_CUSTOM_SERVICE_CHAR_DESCRIPTORS_COUNT  (0x00u)

/* Below are the indexes and handles of the defined Custom Services and their characteristics */
#define CYBLE_ELEALIGHT_SERVICE_INDEX   (0x00u) /* Index of EleaLight service in the cyBle_customs array */
#define CYBLE_ELEALIGHT_FACTORYRESET_CHAR_INDEX   (0x00u) /* Index of FactoryReset characteristic */
#define CYBLE_ELEALIGHT_LEARN_CHAR_INDEX   (0x01u) /* Index of Learn characteristic */
#define CYBLE_ELEALIGHT_FORGET_CHAR_INDEX   (0x02u) /* Index of Forget characteristic */
#define CYBLE_ELEALIGHT_DEVICETYPE_CHAR_INDEX   (0x03u) /* Index of DeviceType characteristic */
#define CYBLE_ELEALIGHT_CFGRED_CHAR_INDEX   (0x04u) /* Index of CfgRed characteristic */
#define CYBLE_ELEALIGHT_CFGGREEN_CHAR_INDEX   (0x05u) /* Index of CfgGreen characteristic */
#define CYBLE_ELEALIGHT_CFGBLUE_CHAR_INDEX   (0x06u) /* Index of CfgBlue characteristic */
#define CYBLE_ELEALIGHT_CFGWHITE_CHAR_INDEX   (0x07u) /* Index of CfgWhite characteristic */
#define CYBLE_ELEALIGHT_CFGYELLOW_CHAR_INDEX   (0x08u) /* Index of CfgYellow characteristic */
#define CYBLE_ELEALIGHT_CFGPINK_CHAR_INDEX   (0x09u) /* Index of CfgPink characteristic */
#define CYBLE_ELEALIGHT_CUSTOMNAME_CHAR_INDEX   (0x0Au) /* Index of CustomName characteristic */


#define CYBLE_ELEALIGHT_SERVICE_HANDLE   (0x000Cu) /* Handle of EleaLight service */
#define CYBLE_ELEALIGHT_FACTORYRESET_DECL_HANDLE   (0x000Du) /* Handle of FactoryReset characteristic declaration */
#define CYBLE_ELEALIGHT_FACTORYRESET_CHAR_HANDLE   (0x000Eu) /* Handle of FactoryReset characteristic */
#define CYBLE_ELEALIGHT_LEARN_DECL_HANDLE   (0x000Fu) /* Handle of Learn characteristic declaration */
#define CYBLE_ELEALIGHT_LEARN_CHAR_HANDLE   (0x0010u) /* Handle of Learn characteristic */
#define CYBLE_ELEALIGHT_FORGET_DECL_HANDLE   (0x0011u) /* Handle of Forget characteristic declaration */
#define CYBLE_ELEALIGHT_FORGET_CHAR_HANDLE   (0x0012u) /* Handle of Forget characteristic */
#define CYBLE_ELEALIGHT_DEVICETYPE_DECL_HANDLE   (0x0013u) /* Handle of DeviceType characteristic declaration */
#define CYBLE_ELEALIGHT_DEVICETYPE_CHAR_HANDLE   (0x0014u) /* Handle of DeviceType characteristic */
#define CYBLE_ELEALIGHT_CFGRED_DECL_HANDLE   (0x0015u) /* Handle of CfgRed characteristic declaration */
#define CYBLE_ELEALIGHT_CFGRED_CHAR_HANDLE   (0x0016u) /* Handle of CfgRed characteristic */
#define CYBLE_ELEALIGHT_CFGGREEN_DECL_HANDLE   (0x0017u) /* Handle of CfgGreen characteristic declaration */
#define CYBLE_ELEALIGHT_CFGGREEN_CHAR_HANDLE   (0x0018u) /* Handle of CfgGreen characteristic */
#define CYBLE_ELEALIGHT_CFGBLUE_DECL_HANDLE   (0x0019u) /* Handle of CfgBlue characteristic declaration */
#define CYBLE_ELEALIGHT_CFGBLUE_CHAR_HANDLE   (0x001Au) /* Handle of CfgBlue characteristic */
#define CYBLE_ELEALIGHT_CFGWHITE_DECL_HANDLE   (0x001Bu) /* Handle of CfgWhite characteristic declaration */
#define CYBLE_ELEALIGHT_CFGWHITE_CHAR_HANDLE   (0x001Cu) /* Handle of CfgWhite characteristic */
#define CYBLE_ELEALIGHT_CFGYELLOW_DECL_HANDLE   (0x001Du) /* Handle of CfgYellow characteristic declaration */
#define CYBLE_ELEALIGHT_CFGYELLOW_CHAR_HANDLE   (0x001Eu) /* Handle of CfgYellow characteristic */
#define CYBLE_ELEALIGHT_CFGPINK_DECL_HANDLE   (0x001Fu) /* Handle of CfgPink characteristic declaration */
#define CYBLE_ELEALIGHT_CFGPINK_CHAR_HANDLE   (0x0020u) /* Handle of CfgPink characteristic */
#define CYBLE_ELEALIGHT_CUSTOMNAME_DECL_HANDLE   (0x0021u) /* Handle of CustomName characteristic declaration */
#define CYBLE_ELEALIGHT_CUSTOMNAME_CHAR_HANDLE   (0x0022u) /* Handle of CustomName characteristic */



#if(CYBLE_CUSTOMS_SERVICE_COUNT != 0u)
    #define CYBLE_CUSTOM_SERVER
#endif /* (CYBLE_CUSTOMS_SERVICE_COUNT != 0u) */
    
#if(CYBLE_CUSTOMC_SERVICE_COUNT != 0u)
    #define CYBLE_CUSTOM_CLIENT
#endif /* (CYBLE_CUSTOMC_SERVICE_COUNT != 0u) */

/***************************************
* Data Struct Definition
***************************************/

/**
 \addtogroup group_service_api_custom
 @{
*/

#ifdef CYBLE_CUSTOM_SERVER

/** Contains information about Custom Characteristic structure */
typedef struct
{
    /** Custom Characteristic handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServCharHandle;
    /** Custom Characteristic Descriptors handles */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServCharDesc[     /* MDK doesn't allow array with zero length */
        CYBLE_CUSTOM_SERVICE_CHAR_DESCRIPTORS_COUNT == 0u ? 1u : CYBLE_CUSTOM_SERVICE_CHAR_DESCRIPTORS_COUNT];
} CYBLE_CUSTOMS_INFO_T;

/** Structure with Custom Service attribute handles. */
typedef struct
{
    /** Handle of a Custom Service */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServHandle;
    
    /** Information about Custom Characteristics */
    CYBLE_CUSTOMS_INFO_T customServInfo[                /* MDK doesn't allow array with zero length */
        CYBLE_CUSTOM_SERVICE_CHAR_COUNT == 0u ? 1u : CYBLE_CUSTOM_SERVICE_CHAR_COUNT];
} CYBLE_CUSTOMS_T;


#endif /* (CYBLE_CUSTOM_SERVER) */

#ifdef CYBLE_CUSTOM_CLIENT

/** Structure with discovered attributes information of Custom Service Descriptors */
typedef struct
{
    /** Custom Descriptor handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T descHandle;
	/** Custom Descriptor 128 bit UUID */
	const void *uuid;           
    /** UUID Format - 16-bit (0x01) or 128-bit (0x02) */
	uint8 uuidFormat;
   
} CYBLE_CUSTOMC_DESC_T;

/** Structure with discovered attributes information of Custom Service Characteristics */
typedef struct
{
    /** Characteristic handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServCharHandle;
	/** Characteristic end handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServCharEndHandle;
	/** Custom Characteristic UUID */
	const void *uuid;           
    /** UUID Format - 16-bit (0x01) or 128-bit (0x02) */
	uint8 uuidFormat;
    /** Properties for value field */
    uint8  properties;
	/** Number of descriptors */
    uint8 descCount;
    /** Characteristic Descriptors */
    CYBLE_CUSTOMC_DESC_T * customServCharDesc;
} CYBLE_CUSTOMC_CHAR_T;

/** Structure with discovered attributes information of Custom Service */
typedef struct
{
    /** Custom Service handle */
    CYBLE_GATT_DB_ATTR_HANDLE_T customServHandle;
	/** Custom Service UUID */
	const void *uuid;           
    /** UUID Format - 16-bit (0x01) or 128-bit (0x02) */
	uint8 uuidFormat;
	/** Number of characteristics */
    uint8 charCount;
    /** Custom Service Characteristics */
    CYBLE_CUSTOMC_CHAR_T * customServChar;
} CYBLE_CUSTOMC_T;

#endif /* (CYBLE_CUSTOM_CLIENT) */


/***************************************
* External data references 
***************************************/

#ifdef CYBLE_CUSTOM_SERVER

/** Custom Services GATT DB handles structures */
extern const CYBLE_CUSTOMS_T cyBle_customs[CYBLE_CUSTOMS_SERVICE_COUNT];

#endif /* (CYBLE_CUSTOM_SERVER) */

#ifdef CYBLE_CUSTOM_CLIENT

/** Custom Services discovered attributes information */
extern CYBLE_CUSTOMC_T cyBle_customCServ[CYBLE_CUSTOMC_SERVICE_COUNT];

#endif /* (CYBLE_CUSTOM_CLIENT) */

/***************************************
* Private Function Prototypes
***************************************/

/** \cond IGNORE */
void CyBle_CustomInit(void);

#ifdef CYBLE_CUSTOM_CLIENT

void CyBle_CustomcDiscoverServiceEventHandler(const CYBLE_DISC_SRVC128_INFO_T *discServInfo);
void CyBle_CustomcDiscoverCharacteristicsEventHandler(uint16 discoveryService, const CYBLE_DISC_CHAR_INFO_T *discCharInfo);
CYBLE_GATT_ATTR_HANDLE_RANGE_T CyBle_CustomcGetCharRange(uint8 incrementIndex);
void CyBle_CustomcDiscoverCharDescriptorsEventHandler(const CYBLE_DISC_DESCR_INFO_T *discDescrInfo);

#endif /* (CYBLE_CUSTOM_CLIENT) */


/***************************************
* The following code is DEPRECATED and
* should not be used in new projects.
***************************************/
#define customServiceCharHandle         customServCharHandle
#define customServiceCharDescriptors    customServCharDesc
#define customServiceHandle             customServHandle
#define customServiceInfo               customServInfo
/** \endcond */

/** @} */

#endif /* CY_BLE_CYBLE_CUSTOM_H  */

/* [] END OF FILE */
