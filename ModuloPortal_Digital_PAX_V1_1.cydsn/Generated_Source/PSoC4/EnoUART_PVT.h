/***************************************************************************//**
* \file .h
* \version 4.0
*
* \brief
*  This private file provides constants and parameter values for the
*  SCB Component.
*  Please do not use this file or its content in your project.
*
* Note:
*
********************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation. All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_SCB_PVT_EnoUART_H)
#define CY_SCB_PVT_EnoUART_H

#include "EnoUART.h"


/***************************************
*     Private Function Prototypes
***************************************/

/* APIs to service INTR_I2C_EC register */
#define EnoUART_SetI2CExtClkInterruptMode(interruptMask) EnoUART_WRITE_INTR_I2C_EC_MASK(interruptMask)
#define EnoUART_ClearI2CExtClkInterruptSource(interruptMask) EnoUART_CLEAR_INTR_I2C_EC(interruptMask)
#define EnoUART_GetI2CExtClkInterruptSource()                (EnoUART_INTR_I2C_EC_REG)
#define EnoUART_GetI2CExtClkInterruptMode()                  (EnoUART_INTR_I2C_EC_MASK_REG)
#define EnoUART_GetI2CExtClkInterruptSourceMasked()          (EnoUART_INTR_I2C_EC_MASKED_REG)

#if (!EnoUART_CY_SCBIP_V1)
    /* APIs to service INTR_SPI_EC register */
    #define EnoUART_SetSpiExtClkInterruptMode(interruptMask) \
                                                                EnoUART_WRITE_INTR_SPI_EC_MASK(interruptMask)
    #define EnoUART_ClearSpiExtClkInterruptSource(interruptMask) \
                                                                EnoUART_CLEAR_INTR_SPI_EC(interruptMask)
    #define EnoUART_GetExtSpiClkInterruptSource()                 (EnoUART_INTR_SPI_EC_REG)
    #define EnoUART_GetExtSpiClkInterruptMode()                   (EnoUART_INTR_SPI_EC_MASK_REG)
    #define EnoUART_GetExtSpiClkInterruptSourceMasked()           (EnoUART_INTR_SPI_EC_MASKED_REG)
#endif /* (!EnoUART_CY_SCBIP_V1) */

#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    extern void EnoUART_SetPins(uint32 mode, uint32 subMode, uint32 uartEnableMask);
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Vars with External Linkage
***************************************/

#if (EnoUART_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_EnoUART_CUSTOM_INTR_HANDLER)
    extern cyisraddress EnoUART_customIntrHandler;
#endif /* !defined (CY_REMOVE_EnoUART_CUSTOM_INTR_HANDLER) */
#endif /* (EnoUART_SCB_IRQ_INTERNAL) */

extern EnoUART_BACKUP_STRUCT EnoUART_backup;

#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    extern uint8 EnoUART_scbMode;
    extern uint8 EnoUART_scbEnableWake;
    extern uint8 EnoUART_scbEnableIntr;

    /* I2C configuration variables */
    extern uint8 EnoUART_mode;
    extern uint8 EnoUART_acceptAddr;

    /* SPI/UART configuration variables */
    extern volatile uint8 * EnoUART_rxBuffer;
    extern uint8   EnoUART_rxDataBits;
    extern uint32  EnoUART_rxBufferSize;

    extern volatile uint8 * EnoUART_txBuffer;
    extern uint8   EnoUART_txDataBits;
    extern uint32  EnoUART_txBufferSize;

    /* EZI2C configuration variables */
    extern uint8 EnoUART_numberOfAddr;
    extern uint8 EnoUART_subAddrSize;
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

#if (! (EnoUART_SCB_MODE_I2C_CONST_CFG || \
        EnoUART_SCB_MODE_EZI2C_CONST_CFG))
    extern uint16 EnoUART_IntrTxMask;
#endif /* (! (EnoUART_SCB_MODE_I2C_CONST_CFG || \
              EnoUART_SCB_MODE_EZI2C_CONST_CFG)) */


/***************************************
*        Conditional Macro
****************************************/

#if(EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Defines run time operation mode */
    #define EnoUART_SCB_MODE_I2C_RUNTM_CFG     (EnoUART_SCB_MODE_I2C      == EnoUART_scbMode)
    #define EnoUART_SCB_MODE_SPI_RUNTM_CFG     (EnoUART_SCB_MODE_SPI      == EnoUART_scbMode)
    #define EnoUART_SCB_MODE_UART_RUNTM_CFG    (EnoUART_SCB_MODE_UART     == EnoUART_scbMode)
    #define EnoUART_SCB_MODE_EZI2C_RUNTM_CFG   (EnoUART_SCB_MODE_EZI2C    == EnoUART_scbMode)
    #define EnoUART_SCB_MODE_UNCONFIG_RUNTM_CFG \
                                                        (EnoUART_SCB_MODE_UNCONFIG == EnoUART_scbMode)

    /* Defines wakeup enable */
    #define EnoUART_SCB_WAKE_ENABLE_CHECK       (0u != EnoUART_scbEnableWake)
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */

/* Defines maximum number of SCB pins */
#if (!EnoUART_CY_SCBIP_V1)
    #define EnoUART_SCB_PINS_NUMBER    (7u)
#else
    #define EnoUART_SCB_PINS_NUMBER    (2u)
#endif /* (!EnoUART_CY_SCBIP_V1) */

#endif /* (CY_SCB_PVT_EnoUART_H) */


/* [] END OF FILE */
