/***************************************************************************//**
* \file EnoUART.c
* \version 4.0
*
* \brief
*  This file provides the source code to the API for the SCB Component.
*
* Note:
*
*******************************************************************************
* \copyright
* Copyright 2013-2017, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions,
* disclaimers, and limitations in the end user license agreement accompanying
* the software package with which this file was provided.
*******************************************************************************/

#include "EnoUART_PVT.h"

#if (EnoUART_SCB_MODE_I2C_INC)
    #include "EnoUART_I2C_PVT.h"
#endif /* (EnoUART_SCB_MODE_I2C_INC) */

#if (EnoUART_SCB_MODE_EZI2C_INC)
    #include "EnoUART_EZI2C_PVT.h"
#endif /* (EnoUART_SCB_MODE_EZI2C_INC) */

#if (EnoUART_SCB_MODE_SPI_INC || EnoUART_SCB_MODE_UART_INC)
    #include "EnoUART_SPI_UART_PVT.h"
#endif /* (EnoUART_SCB_MODE_SPI_INC || EnoUART_SCB_MODE_UART_INC) */


/***************************************
*    Run Time Configuration Vars
***************************************/

/* Stores internal component configuration for Unconfigured mode */
#if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Common configuration variables */
    uint8 EnoUART_scbMode = EnoUART_SCB_MODE_UNCONFIG;
    uint8 EnoUART_scbEnableWake;
    uint8 EnoUART_scbEnableIntr;

    /* I2C configuration variables */
    uint8 EnoUART_mode;
    uint8 EnoUART_acceptAddr;

    /* SPI/UART configuration variables */
    volatile uint8 * EnoUART_rxBuffer;
    uint8  EnoUART_rxDataBits;
    uint32 EnoUART_rxBufferSize;

    volatile uint8 * EnoUART_txBuffer;
    uint8  EnoUART_txDataBits;
    uint32 EnoUART_txBufferSize;

    /* EZI2C configuration variables */
    uint8 EnoUART_numberOfAddr;
    uint8 EnoUART_subAddrSize;
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


/***************************************
*     Common SCB Vars
***************************************/
/**
* \addtogroup group_general
* \{
*/

/** EnoUART_initVar indicates whether the EnoUART 
*  component has been initialized. The variable is initialized to 0 
*  and set to 1 the first time SCB_Start() is called. This allows 
*  the component to restart without reinitialization after the first 
*  call to the EnoUART_Start() routine.
*
*  If re-initialization of the component is required, then the 
*  EnoUART_Init() function can be called before the 
*  EnoUART_Start() or EnoUART_Enable() function.
*/
uint8 EnoUART_initVar = 0u;


#if (! (EnoUART_SCB_MODE_I2C_CONST_CFG || \
        EnoUART_SCB_MODE_EZI2C_CONST_CFG))
    /** This global variable stores TX interrupt sources after 
    * EnoUART_Stop() is called. Only these TX interrupt sources 
    * will be restored on a subsequent EnoUART_Enable() call.
    */
    uint16 EnoUART_IntrTxMask = 0u;
#endif /* (! (EnoUART_SCB_MODE_I2C_CONST_CFG || \
              EnoUART_SCB_MODE_EZI2C_CONST_CFG)) */
/** \} globals */

#if (EnoUART_SCB_IRQ_INTERNAL)
#if !defined (CY_REMOVE_EnoUART_CUSTOM_INTR_HANDLER)
    void (*EnoUART_customIntrHandler)(void) = NULL;
#endif /* !defined (CY_REMOVE_EnoUART_CUSTOM_INTR_HANDLER) */
#endif /* (EnoUART_SCB_IRQ_INTERNAL) */


/***************************************
*    Private Function Prototypes
***************************************/

static void EnoUART_ScbEnableIntr(void);
static void EnoUART_ScbModeStop(void);
static void EnoUART_ScbModePostEnable(void);


/*******************************************************************************
* Function Name: EnoUART_Init
****************************************************************************//**
*
*  Initializes the EnoUART component to operate in one of the selected
*  configurations: I2C, SPI, UART or EZI2C.
*  When the configuration is set to "Unconfigured SCB", this function does
*  not do any initialization. Use mode-specific initialization APIs instead:
*  EnoUART_I2CInit, EnoUART_SpiInit, 
*  EnoUART_UartInit or EnoUART_EzI2CInit.
*
*******************************************************************************/
void EnoUART_Init(void)
{
#if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    if (EnoUART_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        EnoUART_initVar = 0u;
    }
    else
    {
        /* Initialization was done before this function call */
    }

#elif (EnoUART_SCB_MODE_I2C_CONST_CFG)
    EnoUART_I2CInit();

#elif (EnoUART_SCB_MODE_SPI_CONST_CFG)
    EnoUART_SpiInit();

#elif (EnoUART_SCB_MODE_UART_CONST_CFG)
    EnoUART_UartInit();

#elif (EnoUART_SCB_MODE_EZI2C_CONST_CFG)
    EnoUART_EzI2CInit();

#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_Enable
****************************************************************************//**
*
*  Enables EnoUART component operation: activates the hardware and 
*  internal interrupt. It also restores TX interrupt sources disabled after the 
*  EnoUART_Stop() function was called (note that level-triggered TX 
*  interrupt sources remain disabled to not cause code lock-up).
*  For I2C and EZI2C modes the interrupt is internal and mandatory for 
*  operation. For SPI and UART modes the interrupt can be configured as none, 
*  internal or external.
*  The EnoUART configuration should be not changed when the component
*  is enabled. Any configuration changes should be made after disabling the 
*  component.
*  When configuration is set to “Unconfigured EnoUART”, the component 
*  must first be initialized to operate in one of the following configurations: 
*  I2C, SPI, UART or EZ I2C, using the mode-specific initialization API. 
*  Otherwise this function does not enable the component.
*
*******************************************************************************/
void EnoUART_Enable(void)
{
#if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /* Enable SCB block, only if it is already configured */
    if (!EnoUART_SCB_MODE_UNCONFIG_RUNTM_CFG)
    {
        EnoUART_CTRL_REG |= EnoUART_CTRL_ENABLED;

        EnoUART_ScbEnableIntr();

        /* Call PostEnable function specific to current operation mode */
        EnoUART_ScbModePostEnable();
    }
#else
    EnoUART_CTRL_REG |= EnoUART_CTRL_ENABLED;

    EnoUART_ScbEnableIntr();

    /* Call PostEnable function specific to current operation mode */
    EnoUART_ScbModePostEnable();
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_Start
****************************************************************************//**
*
*  Invokes EnoUART_Init() and EnoUART_Enable().
*  After this function call, the component is enabled and ready for operation.
*  When configuration is set to "Unconfigured SCB", the component must first be
*  initialized to operate in one of the following configurations: I2C, SPI, UART
*  or EZI2C. Otherwise this function does not enable the component.
*
* \globalvars
*  EnoUART_initVar - used to check initial configuration, modified
*  on first function call.
*
*******************************************************************************/
void EnoUART_Start(void)
{
    if (0u == EnoUART_initVar)
    {
        EnoUART_Init();
        EnoUART_initVar = 1u; /* Component was initialized */
    }

    EnoUART_Enable();
}


/*******************************************************************************
* Function Name: EnoUART_Stop
****************************************************************************//**
*
*  Disables the EnoUART component: disable the hardware and internal 
*  interrupt. It also disables all TX interrupt sources so as not to cause an 
*  unexpected interrupt trigger because after the component is enabled, the 
*  TX FIFO is empty.
*  Refer to the function EnoUART_Enable() for the interrupt 
*  configuration details.
*  This function disables the SCB component without checking to see if 
*  communication is in progress. Before calling this function it may be 
*  necessary to check the status of communication to make sure communication 
*  is complete. If this is not done then communication could be stopped mid 
*  byte and corrupted data could result.
*
*******************************************************************************/
void EnoUART_Stop(void)
{
#if (EnoUART_SCB_IRQ_INTERNAL)
    EnoUART_DisableInt();
#endif /* (EnoUART_SCB_IRQ_INTERNAL) */

    /* Call Stop function specific to current operation mode */
    EnoUART_ScbModeStop();

    /* Disable SCB IP */
    EnoUART_CTRL_REG &= (uint32) ~EnoUART_CTRL_ENABLED;

    /* Disable all TX interrupt sources so as not to cause an unexpected
    * interrupt trigger after the component will be enabled because the 
    * TX FIFO is empty.
    * For SCB IP v0, it is critical as it does not mask-out interrupt
    * sources when it is disabled. This can cause a code lock-up in the
    * interrupt handler because TX FIFO cannot be loaded after the block
    * is disabled.
    */
    EnoUART_SetTxInterruptMode(EnoUART_NO_INTR_SOURCES);

#if (EnoUART_SCB_IRQ_INTERNAL)
    EnoUART_ClearPendingInt();
#endif /* (EnoUART_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: EnoUART_SetRxFifoLevel
****************************************************************************//**
*
*  Sets level in the RX FIFO to generate a RX level interrupt.
*  When the RX FIFO has more entries than the RX FIFO level an RX level
*  interrupt request is generated.
*
*  \param level: Level in the RX FIFO to generate RX level interrupt.
*   The range of valid level values is between 0 and RX FIFO depth - 1.
*
*******************************************************************************/
void EnoUART_SetRxFifoLevel(uint32 level)
{
    uint32 rxFifoCtrl;

    rxFifoCtrl = EnoUART_RX_FIFO_CTRL_REG;

    rxFifoCtrl &= ((uint32) ~EnoUART_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    rxFifoCtrl |= ((uint32) (EnoUART_RX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    EnoUART_RX_FIFO_CTRL_REG = rxFifoCtrl;
}


/*******************************************************************************
* Function Name: EnoUART_SetTxFifoLevel
****************************************************************************//**
*
*  Sets level in the TX FIFO to generate a TX level interrupt.
*  When the TX FIFO has less entries than the TX FIFO level an TX level
*  interrupt request is generated.
*
*  \param level: Level in the TX FIFO to generate TX level interrupt.
*   The range of valid level values is between 0 and TX FIFO depth - 1.
*
*******************************************************************************/
void EnoUART_SetTxFifoLevel(uint32 level)
{
    uint32 txFifoCtrl;

    txFifoCtrl = EnoUART_TX_FIFO_CTRL_REG;

    txFifoCtrl &= ((uint32) ~EnoUART_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK); /* Clear level mask bits */
    txFifoCtrl |= ((uint32) (EnoUART_TX_FIFO_CTRL_TRIGGER_LEVEL_MASK & level));

    EnoUART_TX_FIFO_CTRL_REG = txFifoCtrl;
}


#if (EnoUART_SCB_IRQ_INTERNAL)
    /*******************************************************************************
    * Function Name: EnoUART_SetCustomInterruptHandler
    ****************************************************************************//**
    *
    *  Registers a function to be called by the internal interrupt handler.
    *  First the function that is registered is called, then the internal interrupt
    *  handler performs any operation such as software buffer management functions
    *  before the interrupt returns.  It is the user's responsibility not to break
    *  the software buffer operations. Only one custom handler is supported, which
    *  is the function provided by the most recent call.
    *  At the initialization time no custom handler is registered.
    *
    *  \param func: Pointer to the function to register.
    *        The value NULL indicates to remove the current custom interrupt
    *        handler.
    *
    *******************************************************************************/
    void EnoUART_SetCustomInterruptHandler(void (*func)(void))
    {
    #if !defined (CY_REMOVE_EnoUART_CUSTOM_INTR_HANDLER)
        EnoUART_customIntrHandler = func; /* Register interrupt handler */
    #else
        if (NULL != func)
        {
            /* Suppress compiler warning */
        }
    #endif /* !defined (CY_REMOVE_EnoUART_CUSTOM_INTR_HANDLER) */
    }
#endif /* (EnoUART_SCB_IRQ_INTERNAL) */


/*******************************************************************************
* Function Name: EnoUART_ScbModeEnableIntr
****************************************************************************//**
*
*  Enables an interrupt for a specific mode.
*
*******************************************************************************/
static void EnoUART_ScbEnableIntr(void)
{
#if (EnoUART_SCB_IRQ_INTERNAL)
    /* Enable interrupt in NVIC */
    #if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
        if (0u != EnoUART_scbEnableIntr)
        {
            EnoUART_EnableInt();
        }

    #else
        EnoUART_EnableInt();

    #endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
#endif /* (EnoUART_SCB_IRQ_INTERNAL) */
}


/*******************************************************************************
* Function Name: EnoUART_ScbModePostEnable
****************************************************************************//**
*
*  Calls the PostEnable function for a specific operation mode.
*
*******************************************************************************/
static void EnoUART_ScbModePostEnable(void)
{
#if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
#if (!EnoUART_CY_SCBIP_V1)
    if (EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        EnoUART_SpiPostEnable();
    }
    else if (EnoUART_SCB_MODE_UART_RUNTM_CFG)
    {
        EnoUART_UartPostEnable();
    }
    else
    {
        /* Unknown mode: do nothing */
    }
#endif /* (!EnoUART_CY_SCBIP_V1) */

#elif (EnoUART_SCB_MODE_SPI_CONST_CFG)
    EnoUART_SpiPostEnable();

#elif (EnoUART_SCB_MODE_UART_CONST_CFG)
    EnoUART_UartPostEnable();

#else
    /* Unknown mode: do nothing */
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


/*******************************************************************************
* Function Name: EnoUART_ScbModeStop
****************************************************************************//**
*
*  Calls the Stop function for a specific operation mode.
*
*******************************************************************************/
static void EnoUART_ScbModeStop(void)
{
#if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    if (EnoUART_SCB_MODE_I2C_RUNTM_CFG)
    {
        EnoUART_I2CStop();
    }
    else if (EnoUART_SCB_MODE_EZI2C_RUNTM_CFG)
    {
        EnoUART_EzI2CStop();
    }
#if (!EnoUART_CY_SCBIP_V1)
    else if (EnoUART_SCB_MODE_SPI_RUNTM_CFG)
    {
        EnoUART_SpiStop();
    }
    else if (EnoUART_SCB_MODE_UART_RUNTM_CFG)
    {
        EnoUART_UartStop();
    }
#endif /* (!EnoUART_CY_SCBIP_V1) */
    else
    {
        /* Unknown mode: do nothing */
    }
#elif (EnoUART_SCB_MODE_I2C_CONST_CFG)
    EnoUART_I2CStop();

#elif (EnoUART_SCB_MODE_EZI2C_CONST_CFG)
    EnoUART_EzI2CStop();

#elif (EnoUART_SCB_MODE_SPI_CONST_CFG)
    EnoUART_SpiStop();

#elif (EnoUART_SCB_MODE_UART_CONST_CFG)
    EnoUART_UartStop();

#else
    /* Unknown mode: do nothing */
#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */
}


#if (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG)
    /*******************************************************************************
    * Function Name: EnoUART_SetPins
    ****************************************************************************//**
    *
    *  Sets the pins settings accordingly to the selected operation mode.
    *  Only available in the Unconfigured operation mode. The mode specific
    *  initialization function calls it.
    *  Pins configuration is set by PSoC Creator when a specific mode of operation
    *  is selected in design time.
    *
    *  \param mode:      Mode of SCB operation.
    *  \param subMode:   Sub-mode of SCB operation. It is only required for SPI and UART
    *             modes.
    *  \param uartEnableMask: enables TX or RX direction and RTS and CTS signals.
    *
    *******************************************************************************/
    void EnoUART_SetPins(uint32 mode, uint32 subMode, uint32 uartEnableMask)
    {
        uint32 pinsDm[EnoUART_SCB_PINS_NUMBER];
        uint32 i;
        
    #if (!EnoUART_CY_SCBIP_V1)
        uint32 pinsInBuf = 0u;
    #endif /* (!EnoUART_CY_SCBIP_V1) */
        
        uint32 hsiomSel[EnoUART_SCB_PINS_NUMBER] = 
        {
            EnoUART_RX_SDA_MOSI_HSIOM_SEL_GPIO,
            EnoUART_TX_SCL_MISO_HSIOM_SEL_GPIO,
            0u,
            0u,
            0u,
            0u,
            0u,
        };

    #if (EnoUART_CY_SCBIP_V1)
        /* Supress compiler warning. */
        if ((0u == subMode) || (0u == uartEnableMask))
        {
        }
    #endif /* (EnoUART_CY_SCBIP_V1) */

        /* Set default HSIOM to GPIO and Drive Mode to Analog Hi-Z */
        for (i = 0u; i < EnoUART_SCB_PINS_NUMBER; i++)
        {
            pinsDm[i] = EnoUART_PIN_DM_ALG_HIZ;
        }

        if ((EnoUART_SCB_MODE_I2C   == mode) ||
            (EnoUART_SCB_MODE_EZI2C == mode))
        {
        #if (EnoUART_RX_SDA_MOSI_PIN)
            hsiomSel[EnoUART_RX_SDA_MOSI_PIN_INDEX] = EnoUART_RX_SDA_MOSI_HSIOM_SEL_I2C;
            pinsDm  [EnoUART_RX_SDA_MOSI_PIN_INDEX] = EnoUART_PIN_DM_OD_LO;
        #elif (EnoUART_RX_WAKE_SDA_MOSI_PIN)
            hsiomSel[EnoUART_RX_WAKE_SDA_MOSI_PIN_INDEX] = EnoUART_RX_WAKE_SDA_MOSI_HSIOM_SEL_I2C;
            pinsDm  [EnoUART_RX_WAKE_SDA_MOSI_PIN_INDEX] = EnoUART_PIN_DM_OD_LO;
        #else
        #endif /* (EnoUART_RX_SDA_MOSI_PIN) */
        
        #if (EnoUART_TX_SCL_MISO_PIN)
            hsiomSel[EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_TX_SCL_MISO_HSIOM_SEL_I2C;
            pinsDm  [EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_PIN_DM_OD_LO;
        #endif /* (EnoUART_TX_SCL_MISO_PIN) */
        }
    #if (!EnoUART_CY_SCBIP_V1)
        else if (EnoUART_SCB_MODE_SPI == mode)
        {
        #if (EnoUART_RX_SDA_MOSI_PIN)
            hsiomSel[EnoUART_RX_SDA_MOSI_PIN_INDEX] = EnoUART_RX_SDA_MOSI_HSIOM_SEL_SPI;
        #elif (EnoUART_RX_WAKE_SDA_MOSI_PIN)
            hsiomSel[EnoUART_RX_WAKE_SDA_MOSI_PIN_INDEX] = EnoUART_RX_WAKE_SDA_MOSI_HSIOM_SEL_SPI;
        #else
        #endif /* (EnoUART_RX_SDA_MOSI_PIN) */
        
        #if (EnoUART_TX_SCL_MISO_PIN)
            hsiomSel[EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_TX_SCL_MISO_HSIOM_SEL_SPI;
        #endif /* (EnoUART_TX_SCL_MISO_PIN) */
        
        #if (EnoUART_CTS_SCLK_PIN)
            hsiomSel[EnoUART_CTS_SCLK_PIN_INDEX] = EnoUART_CTS_SCLK_HSIOM_SEL_SPI;
        #endif /* (EnoUART_CTS_SCLK_PIN) */

            if (EnoUART_SPI_SLAVE == subMode)
            {
                /* Slave */
                pinsDm[EnoUART_RX_SDA_MOSI_PIN_INDEX] = EnoUART_PIN_DM_DIG_HIZ;
                pinsDm[EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_PIN_DM_STRONG;
                pinsDm[EnoUART_CTS_SCLK_PIN_INDEX] = EnoUART_PIN_DM_DIG_HIZ;

            #if (EnoUART_RTS_SS0_PIN)
                /* Only SS0 is valid choice for Slave */
                hsiomSel[EnoUART_RTS_SS0_PIN_INDEX] = EnoUART_RTS_SS0_HSIOM_SEL_SPI;
                pinsDm  [EnoUART_RTS_SS0_PIN_INDEX] = EnoUART_PIN_DM_DIG_HIZ;
            #endif /* (EnoUART_RTS_SS0_PIN) */

            #if (EnoUART_TX_SCL_MISO_PIN)
                /* Disable input buffer */
                 pinsInBuf |= EnoUART_TX_SCL_MISO_PIN_MASK;
            #endif /* (EnoUART_TX_SCL_MISO_PIN) */
            }
            else 
            {
                /* (Master) */
                pinsDm[EnoUART_RX_SDA_MOSI_PIN_INDEX] = EnoUART_PIN_DM_STRONG;
                pinsDm[EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_PIN_DM_DIG_HIZ;
                pinsDm[EnoUART_CTS_SCLK_PIN_INDEX] = EnoUART_PIN_DM_STRONG;

            #if (EnoUART_RTS_SS0_PIN)
                hsiomSel [EnoUART_RTS_SS0_PIN_INDEX] = EnoUART_RTS_SS0_HSIOM_SEL_SPI;
                pinsDm   [EnoUART_RTS_SS0_PIN_INDEX] = EnoUART_PIN_DM_STRONG;
                pinsInBuf |= EnoUART_RTS_SS0_PIN_MASK;
            #endif /* (EnoUART_RTS_SS0_PIN) */

            #if (EnoUART_SS1_PIN)
                hsiomSel [EnoUART_SS1_PIN_INDEX] = EnoUART_SS1_HSIOM_SEL_SPI;
                pinsDm   [EnoUART_SS1_PIN_INDEX] = EnoUART_PIN_DM_STRONG;
                pinsInBuf |= EnoUART_SS1_PIN_MASK;
            #endif /* (EnoUART_SS1_PIN) */

            #if (EnoUART_SS2_PIN)
                hsiomSel [EnoUART_SS2_PIN_INDEX] = EnoUART_SS2_HSIOM_SEL_SPI;
                pinsDm   [EnoUART_SS2_PIN_INDEX] = EnoUART_PIN_DM_STRONG;
                pinsInBuf |= EnoUART_SS2_PIN_MASK;
            #endif /* (EnoUART_SS2_PIN) */

            #if (EnoUART_SS3_PIN)
                hsiomSel [EnoUART_SS3_PIN_INDEX] = EnoUART_SS3_HSIOM_SEL_SPI;
                pinsDm   [EnoUART_SS3_PIN_INDEX] = EnoUART_PIN_DM_STRONG;
                pinsInBuf |= EnoUART_SS3_PIN_MASK;
            #endif /* (EnoUART_SS3_PIN) */

                /* Disable input buffers */
            #if (EnoUART_RX_SDA_MOSI_PIN)
                pinsInBuf |= EnoUART_RX_SDA_MOSI_PIN_MASK;
            #elif (EnoUART_RX_WAKE_SDA_MOSI_PIN)
                pinsInBuf |= EnoUART_RX_WAKE_SDA_MOSI_PIN_MASK;
            #else
            #endif /* (EnoUART_RX_SDA_MOSI_PIN) */

            #if (EnoUART_CTS_SCLK_PIN)
                pinsInBuf |= EnoUART_CTS_SCLK_PIN_MASK;
            #endif /* (EnoUART_CTS_SCLK_PIN) */
            }
        }
        else /* UART */
        {
            if (EnoUART_UART_MODE_SMARTCARD == subMode)
            {
                /* SmartCard */
            #if (EnoUART_TX_SCL_MISO_PIN)
                hsiomSel[EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_TX_SCL_MISO_HSIOM_SEL_UART;
                pinsDm  [EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_PIN_DM_OD_LO;
            #endif /* (EnoUART_TX_SCL_MISO_PIN) */
            }
            else /* Standard or IrDA */
            {
                if (0u != (EnoUART_UART_RX_PIN_ENABLE & uartEnableMask))
                {
                #if (EnoUART_RX_SDA_MOSI_PIN)
                    hsiomSel[EnoUART_RX_SDA_MOSI_PIN_INDEX] = EnoUART_RX_SDA_MOSI_HSIOM_SEL_UART;
                    pinsDm  [EnoUART_RX_SDA_MOSI_PIN_INDEX] = EnoUART_PIN_DM_DIG_HIZ;
                #elif (EnoUART_RX_WAKE_SDA_MOSI_PIN)
                    hsiomSel[EnoUART_RX_WAKE_SDA_MOSI_PIN_INDEX] = EnoUART_RX_WAKE_SDA_MOSI_HSIOM_SEL_UART;
                    pinsDm  [EnoUART_RX_WAKE_SDA_MOSI_PIN_INDEX] = EnoUART_PIN_DM_DIG_HIZ;
                #else
                #endif /* (EnoUART_RX_SDA_MOSI_PIN) */
                }

                if (0u != (EnoUART_UART_TX_PIN_ENABLE & uartEnableMask))
                {
                #if (EnoUART_TX_SCL_MISO_PIN)
                    hsiomSel[EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_TX_SCL_MISO_HSIOM_SEL_UART;
                    pinsDm  [EnoUART_TX_SCL_MISO_PIN_INDEX] = EnoUART_PIN_DM_STRONG;
                    
                    /* Disable input buffer */
                    pinsInBuf |= EnoUART_TX_SCL_MISO_PIN_MASK;
                #endif /* (EnoUART_TX_SCL_MISO_PIN) */
                }

            #if !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
                if (EnoUART_UART_MODE_STD == subMode)
                {
                    if (0u != (EnoUART_UART_CTS_PIN_ENABLE & uartEnableMask))
                    {
                        /* CTS input is multiplexed with SCLK */
                    #if (EnoUART_CTS_SCLK_PIN)
                        hsiomSel[EnoUART_CTS_SCLK_PIN_INDEX] = EnoUART_CTS_SCLK_HSIOM_SEL_UART;
                        pinsDm  [EnoUART_CTS_SCLK_PIN_INDEX] = EnoUART_PIN_DM_DIG_HIZ;
                    #endif /* (EnoUART_CTS_SCLK_PIN) */
                    }

                    if (0u != (EnoUART_UART_RTS_PIN_ENABLE & uartEnableMask))
                    {
                        /* RTS output is multiplexed with SS0 */
                    #if (EnoUART_RTS_SS0_PIN)
                        hsiomSel[EnoUART_RTS_SS0_PIN_INDEX] = EnoUART_RTS_SS0_HSIOM_SEL_UART;
                        pinsDm  [EnoUART_RTS_SS0_PIN_INDEX] = EnoUART_PIN_DM_STRONG;
                        
                        /* Disable input buffer */
                        pinsInBuf |= EnoUART_RTS_SS0_PIN_MASK;
                    #endif /* (EnoUART_RTS_SS0_PIN) */
                    }
                }
            #endif /* !(EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */
            }
        }
    #endif /* (!EnoUART_CY_SCBIP_V1) */

    /* Configure pins: set HSIOM, DM and InputBufEnable */
    /* Note: the DR register settings do not effect the pin output if HSIOM is other than GPIO */

    #if (EnoUART_RX_SDA_MOSI_PIN)
        EnoUART_SET_HSIOM_SEL(EnoUART_RX_SDA_MOSI_HSIOM_REG,
                                       EnoUART_RX_SDA_MOSI_HSIOM_MASK,
                                       EnoUART_RX_SDA_MOSI_HSIOM_POS,
                                        hsiomSel[EnoUART_RX_SDA_MOSI_PIN_INDEX]);

        EnoUART_uart_rx_i2c_sda_spi_mosi_SetDriveMode((uint8) pinsDm[EnoUART_RX_SDA_MOSI_PIN_INDEX]);

        #if (!EnoUART_CY_SCBIP_V1)
            EnoUART_SET_INP_DIS(EnoUART_uart_rx_i2c_sda_spi_mosi_INP_DIS,
                                         EnoUART_uart_rx_i2c_sda_spi_mosi_MASK,
                                         (0u != (pinsInBuf & EnoUART_RX_SDA_MOSI_PIN_MASK)));
        #endif /* (!EnoUART_CY_SCBIP_V1) */
    
    #elif (EnoUART_RX_WAKE_SDA_MOSI_PIN)
        EnoUART_SET_HSIOM_SEL(EnoUART_RX_WAKE_SDA_MOSI_HSIOM_REG,
                                       EnoUART_RX_WAKE_SDA_MOSI_HSIOM_MASK,
                                       EnoUART_RX_WAKE_SDA_MOSI_HSIOM_POS,
                                       hsiomSel[EnoUART_RX_WAKE_SDA_MOSI_PIN_INDEX]);

        EnoUART_uart_rx_wake_i2c_sda_spi_mosi_SetDriveMode((uint8)
                                                               pinsDm[EnoUART_RX_WAKE_SDA_MOSI_PIN_INDEX]);

        EnoUART_SET_INP_DIS(EnoUART_uart_rx_wake_i2c_sda_spi_mosi_INP_DIS,
                                     EnoUART_uart_rx_wake_i2c_sda_spi_mosi_MASK,
                                     (0u != (pinsInBuf & EnoUART_RX_WAKE_SDA_MOSI_PIN_MASK)));

         /* Set interrupt on falling edge */
        EnoUART_SET_INCFG_TYPE(EnoUART_RX_WAKE_SDA_MOSI_INTCFG_REG,
                                        EnoUART_RX_WAKE_SDA_MOSI_INTCFG_TYPE_MASK,
                                        EnoUART_RX_WAKE_SDA_MOSI_INTCFG_TYPE_POS,
                                        EnoUART_INTCFG_TYPE_FALLING_EDGE);
    #else
    #endif /* (EnoUART_RX_WAKE_SDA_MOSI_PIN) */

    #if (EnoUART_TX_SCL_MISO_PIN)
        EnoUART_SET_HSIOM_SEL(EnoUART_TX_SCL_MISO_HSIOM_REG,
                                       EnoUART_TX_SCL_MISO_HSIOM_MASK,
                                       EnoUART_TX_SCL_MISO_HSIOM_POS,
                                        hsiomSel[EnoUART_TX_SCL_MISO_PIN_INDEX]);

        EnoUART_uart_tx_i2c_scl_spi_miso_SetDriveMode((uint8) pinsDm[EnoUART_TX_SCL_MISO_PIN_INDEX]);

    #if (!EnoUART_CY_SCBIP_V1)
        EnoUART_SET_INP_DIS(EnoUART_uart_tx_i2c_scl_spi_miso_INP_DIS,
                                     EnoUART_uart_tx_i2c_scl_spi_miso_MASK,
                                    (0u != (pinsInBuf & EnoUART_TX_SCL_MISO_PIN_MASK)));
    #endif /* (!EnoUART_CY_SCBIP_V1) */
    #endif /* (EnoUART_RX_SDA_MOSI_PIN) */

    #if (EnoUART_CTS_SCLK_PIN)
        EnoUART_SET_HSIOM_SEL(EnoUART_CTS_SCLK_HSIOM_REG,
                                       EnoUART_CTS_SCLK_HSIOM_MASK,
                                       EnoUART_CTS_SCLK_HSIOM_POS,
                                       hsiomSel[EnoUART_CTS_SCLK_PIN_INDEX]);

        EnoUART_uart_cts_spi_sclk_SetDriveMode((uint8) pinsDm[EnoUART_CTS_SCLK_PIN_INDEX]);

        EnoUART_SET_INP_DIS(EnoUART_uart_cts_spi_sclk_INP_DIS,
                                     EnoUART_uart_cts_spi_sclk_MASK,
                                     (0u != (pinsInBuf & EnoUART_CTS_SCLK_PIN_MASK)));
    #endif /* (EnoUART_CTS_SCLK_PIN) */

    #if (EnoUART_RTS_SS0_PIN)
        EnoUART_SET_HSIOM_SEL(EnoUART_RTS_SS0_HSIOM_REG,
                                       EnoUART_RTS_SS0_HSIOM_MASK,
                                       EnoUART_RTS_SS0_HSIOM_POS,
                                       hsiomSel[EnoUART_RTS_SS0_PIN_INDEX]);

        EnoUART_uart_rts_spi_ss0_SetDriveMode((uint8) pinsDm[EnoUART_RTS_SS0_PIN_INDEX]);

        EnoUART_SET_INP_DIS(EnoUART_uart_rts_spi_ss0_INP_DIS,
                                     EnoUART_uart_rts_spi_ss0_MASK,
                                     (0u != (pinsInBuf & EnoUART_RTS_SS0_PIN_MASK)));
    #endif /* (EnoUART_RTS_SS0_PIN) */

    #if (EnoUART_SS1_PIN)
        EnoUART_SET_HSIOM_SEL(EnoUART_SS1_HSIOM_REG,
                                       EnoUART_SS1_HSIOM_MASK,
                                       EnoUART_SS1_HSIOM_POS,
                                       hsiomSel[EnoUART_SS1_PIN_INDEX]);

        EnoUART_spi_ss1_SetDriveMode((uint8) pinsDm[EnoUART_SS1_PIN_INDEX]);

        EnoUART_SET_INP_DIS(EnoUART_spi_ss1_INP_DIS,
                                     EnoUART_spi_ss1_MASK,
                                     (0u != (pinsInBuf & EnoUART_SS1_PIN_MASK)));
    #endif /* (EnoUART_SS1_PIN) */

    #if (EnoUART_SS2_PIN)
        EnoUART_SET_HSIOM_SEL(EnoUART_SS2_HSIOM_REG,
                                       EnoUART_SS2_HSIOM_MASK,
                                       EnoUART_SS2_HSIOM_POS,
                                       hsiomSel[EnoUART_SS2_PIN_INDEX]);

        EnoUART_spi_ss2_SetDriveMode((uint8) pinsDm[EnoUART_SS2_PIN_INDEX]);

        EnoUART_SET_INP_DIS(EnoUART_spi_ss2_INP_DIS,
                                     EnoUART_spi_ss2_MASK,
                                     (0u != (pinsInBuf & EnoUART_SS2_PIN_MASK)));
    #endif /* (EnoUART_SS2_PIN) */

    #if (EnoUART_SS3_PIN)
        EnoUART_SET_HSIOM_SEL(EnoUART_SS3_HSIOM_REG,
                                       EnoUART_SS3_HSIOM_MASK,
                                       EnoUART_SS3_HSIOM_POS,
                                       hsiomSel[EnoUART_SS3_PIN_INDEX]);

        EnoUART_spi_ss3_SetDriveMode((uint8) pinsDm[EnoUART_SS3_PIN_INDEX]);

        EnoUART_SET_INP_DIS(EnoUART_spi_ss3_INP_DIS,
                                     EnoUART_spi_ss3_MASK,
                                     (0u != (pinsInBuf & EnoUART_SS3_PIN_MASK)));
    #endif /* (EnoUART_SS3_PIN) */
    }

#endif /* (EnoUART_SCB_MODE_UNCONFIG_CONST_CFG) */


#if (EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1)
    /*******************************************************************************
    * Function Name: EnoUART_I2CSlaveNackGeneration
    ****************************************************************************//**
    *
    *  Sets command to generate NACK to the address or data.
    *
    *******************************************************************************/
    void EnoUART_I2CSlaveNackGeneration(void)
    {
        /* Check for EC_AM toggle condition: EC_AM and clock stretching for address are enabled */
        if ((0u != (EnoUART_CTRL_REG & EnoUART_CTRL_EC_AM_MODE)) &&
            (0u == (EnoUART_I2C_CTRL_REG & EnoUART_I2C_CTRL_S_NOT_READY_ADDR_NACK)))
        {
            /* Toggle EC_AM before NACK generation */
            EnoUART_CTRL_REG &= ~EnoUART_CTRL_EC_AM_MODE;
            EnoUART_CTRL_REG |=  EnoUART_CTRL_EC_AM_MODE;
        }

        EnoUART_I2C_SLAVE_CMD_REG = EnoUART_I2C_SLAVE_CMD_S_NACK;
    }
#endif /* (EnoUART_CY_SCBIP_V0 || EnoUART_CY_SCBIP_V1) */


/* [] END OF FILE */
