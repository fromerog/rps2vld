/*******************************************************************************
* File Name: EnoPWR.h  
* Version 2.20
*
* Description:
*  This file contains the Alias definitions for Per-Pin APIs in cypins.h. 
*  Information on using these APIs can be found in the System Reference Guide.
*
* Note:
*
********************************************************************************
* Copyright 2008-2015, Cypress Semiconductor Corporation.  All rights reserved.
* You may use this file only in accordance with the license, terms, conditions, 
* disclaimers, and limitations in the end user license agreement accompanying 
* the software package with which this file was provided.
*******************************************************************************/

#if !defined(CY_PINS_EnoPWR_ALIASES_H) /* Pins EnoPWR_ALIASES_H */
#define CY_PINS_EnoPWR_ALIASES_H

#include "cytypes.h"
#include "cyfitter.h"
#include "cypins.h"


/***************************************
*              Constants        
***************************************/
#define EnoPWR_0			(EnoPWR__0__PC)
#define EnoPWR_0_PS		(EnoPWR__0__PS)
#define EnoPWR_0_PC		(EnoPWR__0__PC)
#define EnoPWR_0_DR		(EnoPWR__0__DR)
#define EnoPWR_0_SHIFT	(EnoPWR__0__SHIFT)
#define EnoPWR_0_INTR	((uint16)((uint16)0x0003u << (EnoPWR__0__SHIFT*2u)))

#define EnoPWR_INTR_ALL	 ((uint16)(EnoPWR_0_INTR))


#endif /* End Pins EnoPWR_ALIASES_H */


/* [] END OF FILE */
