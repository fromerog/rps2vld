/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "memory.h"

/* Loads configuration from flash memory into global variables */

void erase_all()
{
    reset_flash();
    load_IDs();
//    load_config();
}

void load_IDs()
{
    uint8 i;
    
    /* VACIA LOS ARRAYS */
    for(i = 0u; i < CY_FLASH_SIZEOF_ROW; i++)
    {
        IDs_RECOVERED_ROJO[i] = 0x00;
    }
        
    
    ADDR_ROW_ROJO = CY_FLASH_ROW_ROJO * CY_FLASH_SIZEOF_ROW;
       
    
    /* CARGA IDs CANAL ROJO */
    for (i = 0u; i < CY_FLASH_SIZEOF_ROW; i++)
    {
        sourceROJO = (*((uint8 *) (ADDR_ROW_ROJO + i)));
        IDs_RECOVERED_ROJO[i] = sourceROJO;
    }
    
}

void reset_flash()
{
    cystatus returnROJO;
    uint8 i = 0;
  
    /* VACIA LOS ARRAYS */
    for(i = 0u; i < CY_FLASH_SIZEOF_ROW; i++)
    {
        IDs_RECOVERED_ROJO[i] = 0x00;
    }

    /* PONE A 0 LAS FILAS DE LA FLASH */
    
    returnROJO = CySysFlashWriteRow(CY_FLASH_ROW_ROJO, IDs_RECOVERED_ROJO);
}

void save_flash(void)
{
    cystatus returnROJO;
    
    returnROJO = CySysFlashWriteRow(CY_FLASH_ROW_ROJO, IDs_RECOVERED_ROJO);
    
    
}

uint8 learn_ID (struct EnoTelegram telegram)
{
    uint8 i;
    uint8 learned = ERROR;

    for (i = 0 ; i<40 ; i = i+5 )
    {
        if ((IDs_RECOVERED_ROJO[i] == 0x00) && (IDs_RECOVERED_ROJO[i+1] == 0x00) &&
            (IDs_RECOVERED_ROJO[i+2] == 0x00) && (IDs_RECOVERED_ROJO[i+3] == 0x00) && (IDs_RECOVERED_ROJO[i+4] == 0x00))
        {
            IDs_RECOVERED_ROJO[i] = telegram.ID_TX[0];
            IDs_RECOVERED_ROJO[i+1] = telegram.ID_TX[1];
            IDs_RECOVERED_ROJO[i+2] = telegram.ID_TX[2];
            IDs_RECOVERED_ROJO[i+3] = telegram.ID_TX[3];
            IDs_RECOVERED_ROJO[i+4] = telegram.DATA[0];
            learned = OK;
            break;
        }
    }

        
    save_flash();
    load_IDs();
    
    return learned;
}

void forget_one_ID (struct EnoTelegram telegram)
{
    uint8 i;
    
    for (i = 0 ; i<40 ; i = i+5 )
            {
                if ((IDs_RECOVERED_ROJO[i] == telegram.ID_TX[0]) && (IDs_RECOVERED_ROJO[i+1] == telegram.ID_TX[1]) &&
                    (IDs_RECOVERED_ROJO[i+2] == telegram.ID_TX[2]) && (IDs_RECOVERED_ROJO[i+3] == telegram.ID_TX[3]) && (IDs_RECOVERED_ROJO[i+4] == telegram.DATA[0]))
                {
                    IDs_RECOVERED_ROJO[i] = 0x00;
                    IDs_RECOVERED_ROJO[i+1] = 0x00;
                    IDs_RECOVERED_ROJO[i+2] = 0x00;
                    IDs_RECOVERED_ROJO[i+3] = 0x00;
                    IDs_RECOVERED_ROJO[i+4] = 0x00;
                }
            }
            
    save_flash();
    load_IDs();        
            
}

void forget_all_ID(void)
{
    reset_flash();
    load_IDs();
    
}

uint8 check_telegram_ID(struct EnoTelegram telegram)
{
    uint8 i;
    
    for (i = 0 ; i<40 ; i = i+5 )
            {
                if ((telegram.ID_TX[0] == 0x00) && (telegram.ID_TX[1] == 0x00) && (telegram.ID_TX[2] == 0x00) && (telegram.ID_TX[3] == 0x00) && (telegram.DATA[0]== 0x00))
                {
                    return 0;
                }
                
                if ((IDs_RECOVERED_ROJO[i] == telegram.ID_TX[0]) && (IDs_RECOVERED_ROJO[i+1] == telegram.ID_TX[1]) &&
                    (IDs_RECOVERED_ROJO[i+2] == telegram.ID_TX[2]) && (IDs_RECOVERED_ROJO[i+3] == telegram.ID_TX[3])) 
                {
                    return 1;
                }
                
            }
            
    return 0;
       
}


/* [] END OF FILE */
